#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PLAYERS 1000
#define MAX_LINE_LENGTH 1000
#define MAX_FIELD_LENGTH 100

typedef struct {
    char name[MAX_FIELD_LENGTH];
    char club[MAX_FIELD_LENGTH];
    int age;
    int potential;
    char photo_url[MAX_FIELD_LENGTH];
} Player;

int main() {

    // download and extract dataset
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");

    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("File not found.\n");
        return 1;
    }

    Player players[MAX_PLAYERS];
    char line[MAX_LINE_LENGTH];
    char field[MAX_FIELD_LENGTH];

    // Skip the header line
    fgets(line, sizeof(line), file);

    int playerCount = 0;
    while (fgets(line, sizeof(line), file)) {
        if (playerCount >= MAX_PLAYERS) {
            printf("Maximum player limit reached.\n");
            break;
        }

        int fieldCount = 0;
        char *token = strtok(line, ",");
        while (token != NULL) {
            switch (fieldCount) {
                case 1:
                    strcpy(players[playerCount].name, token);
                    break;
                case 8:
                    strcpy(players[playerCount].club, token);
                    break;
                case 2:
                    players[playerCount].age = atoi(token);
                    break;
                case 7:
                    players[playerCount].potential = atoi(token);
                    break;
                case 3:
                    strcpy(players[playerCount].photo_url, token);
                    break;
            }
            token = strtok(NULL, ",");
            fieldCount++;
        }

        if (players[playerCount].age < 25 && players[playerCount].potential > 85 && strcmp(players[playerCount].club, "Manchester City") != 0) {
            // Print player information
            printf("Name: %s\n", players[playerCount].name);
            printf("Club: %s\n", players[playerCount].club);
            printf("Age: %d\n", players[playerCount].age);
            printf("Potential: %d\n", players[playerCount].potential);
            printf("Photo URL: %s\n", players[playerCount].photo_url);
            printf("----------------------\n");
        }

        playerCount++;
    }

    fclose(file);
    return 0;
}
