#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>

const char *pathToList = "/home/nutterrz/Documents/sisop/modul_5/soal_2/nanaxgerma/src_data/germaa/projects/restricted_list";
const char *pathToLog = "/home/nutterrz/Documents/sisop/modul_5/soal_2/logmucatatdisini.txt";

void writeLog(char *status, char *cmd, char *desc)
{
    char currDir[PATH_MAX];
    getcwd(currDir, sizeof(currDir));

    int chdirRes = chdir(pathToLog);
    if (chdirRes == -1) {
        perror("gagal merubah direktori");
        exit(1);
    }

    time_t curr_time;
    time(&curr_time);
    struct tm *local_time = localtime(&curr_time);

    char timestamp[20];
    sprintf(timestamp, "%d/%d/%d-%d:%d:%d", local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year + 1900,  local_time->tm_hour, local_time->tm_min, local_time->tm_sec);

    FILE *logFile = fopen(pathToLog, "a");

    if (logFile == NULL) {
        perror("error saat log");
        exit(1);
    }

    fprintf(logFile, "%s::%s::%s::%s\n", status, timestamp, cmd, desc);
    fclose(logFile);

    chdirRes = chdir(currDir);
    if (chdirRes == -1) {
        perror("gagal merubah direktori");
        exit(1);
    }

    exit(0);
}

static int germa_mkdir(char *path, mode_t mode)
{
     char fullPath[PATH_MAX];
    snprintf(fullPath, sizeof(fullPath), "%s%s", pathToList, path);

    int result = mkdir(fullPath, mode);
    if (result == -1) {
        writeLog("FAILED", "MKDIR", path);
        return -errno;
    }

    writeLog("SUCCESS", "MKDIR", path);
    return 0;
}

static int germa_rename(char *oldpath, char *newpath)
{
    char oldFullPath[PATH_MAX];
    char newFullPath[PATH_MAX];
    snprintf(oldFullPath, sizeof(oldFullPath), "%s%s", pathToList, oldpath);
    snprintf(newFullPath, sizeof(newFullPath), "%s%s", pathToList, newpath);

    int result = rename(oldFullPath, newFullPath);
    if (result == -1) {
        writeLog("FAILED", "RENAME", oldpath);
        return -errno;
    }

    writeLog("SUCCESS", "RENAME", oldpath);
    return 0;
}

static int germa_rmdir(char *path)
{
    char fullPath[PATH_MAX];
    snprintf(fullPath, sizeof(fullPath), "%s%s", pathToList, path);

    int result = rmdir(fullPath);
    if (result == -1) {
        writeLog("FAILED", "RMDIR", path);
        return -errno;
    }

    writeLog("SUCCESS", "RMDIR", path);
    return 0;
}

static int germa_unlink(char *path)
{
    char fullPath[PATH_MAX];
    snprintf(fullPath, sizeof(fullPath), "%s%s", pathToList, path);

    int result = unlink(fullPath);
    if (result == -1) {
        writeLog("FAILED", "RMFILE", path);
        return -errno;
    }

    writeLog("SUCCESS", "RMFILE", path);
    return 0;
}

static struct fuse_operations germa_operations = {
    .mkdir = germa_mkdir,
    .rename = germa_rename,
    .rmdir = germa_rmdir,
    .unlink = germa_unlink,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &germa_operations, NULL);
}
