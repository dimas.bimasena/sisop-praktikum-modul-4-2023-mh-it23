# Laporan Resmi Modul 4 Sistem Operasi
Adimasdefatra Bimasena 5027211040

# Soal 1

Kode yang diberikan adalah sebuah program dalam bahasa C yang membaca data pemain sepak bola dari file CSV, memfilter pemain berdasarkan beberapa kriteria, dan mencetak informasi pemain yang memenuhi kriteria tersebut.

Berikut adalah penjelasan mengenai beberapa bagian penting dalam kode tersebut:

Direktif Preprocessor:

#include <stdio.h>: Mengimpor library standar input/output untuk digunakan dalam program.
#include <stdlib.h>: Mengimpor library standar untuk fungsi umum, termasuk fungsi system yang digunakan untuk menjalankan perintah sistem.
#include <string.h>: Mengimpor library standar untuk fungsi pemrosesan string.
Konstanta:

MAX_PLAYERS: Menyatakan batas maksimum jumlah pemain dalam array players.
MAX_LINE_LENGTH: Menyatakan panjang maksimum sebuah baris dalam file CSV yang akan dibaca.
MAX_FIELD_LENGTH: Menyatakan panjang maksimum sebuah field (kolom) dalam file CSV.
Tipe Data:

Player: Tipe data struct yang berisi informasi tentang seorang pemain sepak bola, termasuk nama, klub, usia, potensi, dan URL foto.
Fungsi Utama (main):

Pertama-tama, program mencoba untuk mengunduh dan mengekstrak dataset dengan menggunakan perintah sistem system("kaggle datasets download -d bryanb/fifa-player-stats-database") dan system("unzip fifa-player-stats-database.zip"). Dua perintah ini bertujuan untuk mendapatkan file dataset yang diperlukan.
Kemudian, program membuka file FIFA23_official_data.csv dengan mode "r" (baca) menggunakan fungsi fopen. Jika file tidak ditemukan, program akan mencetak pesan error dan keluar dari program.
Selanjutnya, program mendefinisikan array players yang akan digunakan untuk menyimpan data pemain. Juga, didefinisikan beberapa variabel lokal seperti line untuk membaca baris dari file, dan field untuk membaca nilai dari setiap kolom dalam baris.
Program membaca dan melewatkan baris header pertama menggunakan fungsi fgets, sehingga baris pertama yang berisi label kolom dilewati.
Selama ada baris yang dapat dibaca dari file, program melakukan iterasi.
Di dalam loop, program memproses setiap baris dengan membaginya menjadi kolom-kolom menggunakan fungsi strtok dengan pemisah koma (",") sebagai argumennya.
Setiap nilai kolom yang relevan (nama, klub, usia, potensi, dan URL foto) disalin ke struktur Player yang sesuai.
Setelah semua kolom diproses, program memeriksa apakah pemain memenuhi kriteria yang ditentukan: usia kurang dari 25 tahun, potensi lebih dari 85, dan bukan bermain untuk klub "Manchester City". Jika pemain memenuhi kriteria, informasi pemain dicetak.
Setelah pemrosesan baris selesai, variabel playerCount yang menyimpan jumlah pemain yang telah diproses dan memenuhi kriteria, akan ditambah satu.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PLAYERS 1000
#define MAX_LINE_LENGTH 1000
#define MAX_FIELD_LENGTH 100

typedef struct {
    char name[MAX_FIELD_LENGTH];
    char club[MAX_FIELD_LENGTH];
    int age;
    int potential;
    char photo_url[MAX_FIELD_LENGTH];
} Player;

int main() {

    // download and extract dataset
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");

    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("File not found.\n");
        return 1;
    }

    Player players[MAX_PLAYERS];
    char line[MAX_LINE_LENGTH];
    char field[MAX_FIELD_LENGTH];

    // Skip the header line
    fgets(line, sizeof(line), file);

    int playerCount = 0;
    while (fgets(line, sizeof(line), file)) {
        if (playerCount >= MAX_PLAYERS) {
            printf("Maximum player limit reached.\n");
            break;
        }

        int fieldCount = 0;
        char *token = strtok(line, ",");
        while (token != NULL) {
            switch (fieldCount) {
                case 1:
                    strcpy(players[playerCount].name, token);
                    break;
                case 8:
                    strcpy(players[playerCount].club, token);
                    break;
                case 2:
                    players[playerCount].age = atoi(token);
                    break;
                case 7:
                    players[playerCount].potential = atoi(token);
                    break;
                case 3:
                    strcpy(players[playerCount].photo_url, token);
                    break;
            }
            token = strtok(NULL, ",");
            fieldCount++;
        }

        if (players[playerCount].age < 25 && players[playerCount].potential > 85 && strcmp(players[playerCount].club, "Manchester City") != 0) {
            // Print player information
            printf("Name: %s\n", players[playerCount].name);
            printf("Club: %s\n", players[playerCount].club);
            printf("Age: %d\n", players[playerCount].age);
            printf("Potential: %d\n", players[playerCount].potential);
            printf("Photo URL: %s\n", players[playerCount].photo_url);
            printf("----------------------\n");
        }

        playerCount++;
    }

    fclose(file);
    return 0;
}


# Soal 2

Kode yang diberikan adalah implementasi FUSE (Filesystem in Userspace) yang membuat sebuah filesystem yang terhubung dengan beberapa operasi sistem yang diberikan, seperti membuat direktori, mengganti nama, menghapus direktori, dan menghapus file. Filesystem ini memiliki fungsi untuk mencatat log ke file log yang ditentukan.

Berikut adalah penjelasan mengenai beberapa bagian penting dalam kode tersebut:

Direktif Preprocessor:

#define FUSE_USE_VERSION 30: Mengatur versi FUSE yang akan digunakan.
#include <fuse.h>: Mengimpor library FUSE untuk mengimplementasikan fungsi filesystem.
#include <stdio.h>: Mengimpor library standar input/output untuk digunakan dalam program.
#include <string.h>: Mengimpor library standar untuk fungsi pemrosesan string.
#include <unistd.h>: Mengimpor library standar untuk fungsi sistem dan konstanta POSIX.
#include <fcntl.h>: Mengimpor library standar untuk fungsi sistem terkait pengaturan file.
#include <dirent.h>: Mengimpor library standar untuk fungsi sistem terkait direktori.
#include <errno.h>: Mengimpor library standar untuk variabel errno dan pesan error.
#include <sys/stat.h>: Mengimpor library standar untuk tipe data dan fungsi terkait struktur stat.
#include <sys/time.h>: Mengimpor library standar untuk tipe data terkait waktu.
#include <time.h>: Mengimpor library standar untuk fungsi terkait waktu.
Variabel Konstan:

pathToList: Path menuju direktori yang digunakan untuk menyimpan daftar terbatas.
pathToLog: Path menuju file log yang digunakan untuk mencatat log.
Fungsi writeLog:

Fungsi ini digunakan untuk menulis log ke file log yang ditentukan.
Fungsi ini menerima tiga argumen string: status (status log), cmd (perintah yang dilakukan), dan desc (deskripsi atau path yang berkaitan).
Fungsi ini menggunakan getcwd untuk mendapatkan current working directory saat ini.
Fungsi ini menggunakan chdir untuk berpindah ke direktori file log yang ditentukan.
Fungsi ini menggunakan time dan localtime untuk mendapatkan timestamp saat ini.
Fungsi ini menggunakan fprintf untuk menulis log ke file log.
Setelah menulis log selesai, fungsi ini kembali ke direktori awal menggunakan chdir.
Fungsi-fungsi Operasi FUSE:

germa_mkdir: Fungsi ini dipanggil saat membuat direktori dalam filesystem. Menggunakan mkdir untuk membuat direktori baru. Jika berhasil, akan mencatat log dengan status "SUCCESS", perintah "MKDIR", dan deskripsi path yang berkaitan. Jika gagal, akan mencatat log dengan status "FAILED" dan pesan error.
germa_rename: Fungsi ini dipanggil saat mengganti nama sebuah file atau direktori dalam filesystem. Menggunakan rename untuk mengubah nama path. Jika berhasil, akan mencatat log dengan status "SUCCESS

#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>

const char *pathToList = "/home/nutterrz/Documents/sisop/modul_5/soal_2/nanaxgerma/src_data/germaa/projects/restricted_list";
const char *pathToLog = "/home/nutterrz/Documents/sisop/modul_5/soal_2/logmucatatdisini.txt";

void writeLog(char *status, char *cmd, char *desc)
{
    char currDir[PATH_MAX];
    getcwd(currDir, sizeof(currDir));

    int chdirRes = chdir(pathToLog);
    if (chdirRes == -1) {
        perror("gagal merubah direktori");
        exit(1);
    }

    time_t curr_time;
    time(&curr_time);
    struct tm *local_time = localtime(&curr_time);

    char timestamp[20];
    sprintf(timestamp, "%d/%d/%d-%d:%d:%d", local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year + 1900,  local_time->tm_hour, local_time->tm_min, local_time->tm_sec);

    FILE *logFile = fopen(pathToLog, "a");

    if (logFile == NULL) {
        perror("error saat log");
        exit(1);
    }

    fprintf(logFile, "%s::%s::%s::%s\n", status, timestamp, cmd, desc);
    fclose(logFile);

    chdirRes = chdir(currDir);
    if (chdirRes == -1) {
        perror("gagal merubah direktori");
        exit(1);
    }

    exit(0);
}

static int germa_mkdir(char *path, mode_t mode)
{
     char fullPath[PATH_MAX];
    snprintf(fullPath, sizeof(fullPath), "%s%s", pathToList, path);

    int result = mkdir(fullPath, mode);
    if (result == -1) {
        writeLog("FAILED", "MKDIR", path);
        return -errno;
    }

    writeLog("SUCCESS", "MKDIR", path);
    return 0;
}

static int germa_rename(char *oldpath, char *newpath)
{
    char oldFullPath[PATH_MAX];
    char newFullPath[PATH_MAX];
    snprintf(oldFullPath, sizeof(oldFullPath), "%s%s", pathToList, oldpath);
    snprintf(newFullPath, sizeof(newFullPath), "%s%s", pathToList, newpath);

    int result = rename(oldFullPath, newFullPath);
    if (result == -1) {
        writeLog("FAILED", "RENAME", oldpath);
        return -errno;
    }

    writeLog("SUCCESS", "RENAME", oldpath);
    return 0;
}

static int germa_rmdir(char *path)
{
    char fullPath[PATH_MAX];
    snprintf(fullPath, sizeof(fullPath), "%s%s", pathToList, path);

    int result = rmdir(fullPath);
    if (result == -1) {
        writeLog("FAILED", "RMDIR", path);
        return -errno;
    }

    writeLog("SUCCESS", "RMDIR", path);
    return 0;
}

static int germa_unlink(char *path)
{
    char fullPath[PATH_MAX];
    snprintf(fullPath, sizeof(fullPath), "%s%s", pathToList, path);

    int result = unlink(fullPath);
    if (result == -1) {
        writeLog("FAILED", "RMFILE", path);
        return -errno;
    }

    writeLog("SUCCESS", "RMFILE", path);
    return 0;
}

static struct fuse_operations germa_operations = {
    .mkdir = germa_mkdir,
    .rename = germa_rename,
    .rmdir = germa_rmdir,
    .unlink = germa_unlink,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &germa_operations, NULL);
}


# Soal 5

Kode tersebut merupakan implementasi sebuah FUSE (Filesystem in Userspace) yang memungkinkan untuk membuat dan mengatur sistem file virtual dalam lingkungan pengguna. FUSE memungkinkan pengembang untuk membuat sistem file khusus yang dapat diakses dan dikelola melalui direktori virtual yang terhubung ke sistem operasi.

Berikut adalah penjelasan mengenai kode tersebut:

Pada bagian awal, terdapat beberapa #include yang mengimpor beberapa header file yang dibutuhkan untuk operasi file dan FUSE.
Kemudian, didefinisikan beberapa konstanta seperti path ke folder rahasia, path ke file pengguna, dan path ke file hasil.
Selanjutnya, terdapat array extensions yang berisi daftar ekstensi file yang akan dihitung jumlahnya.
Variabel is_authenticated digunakan untuk menyimpan status autentikasi pengguna.
Fungsi isUserFileExist() digunakan untuk memeriksa apakah file pengguna sudah ada.
Fungsi calculateMD5() digunakan untuk menghitung hash MD5 dari suatu string.
Fungsi isFileDownloaded() digunakan untuk memeriksa apakah file sudah diunduh.
Fungsi downloadAndUnzipFile() digunakan untuk mengunduh dan mengekstrak file "rahasia.zip" jika belum ada.
Fungsi countFilesRecursive() digunakan untuk menghitung jumlah file dengan ekstensi tertentu secara rekursif di dalam suatu direktori.
Fungsi countFilesByExtension() digunakan untuk menghitung jumlah file dengan ekstensi tertentu dalam folder rahasia dan menyimpannya ke dalam file "extension.txt".
Fungsi-fungsi customGetAttr(), customRename(), customReaddir(), dan customRead() merupakan implementasi operasi FUSE untuk mendapatkan atribut file, mengubah nama file, membaca direktori, dan membaca isi file.
Struct fuse_operations digunakan untuk mendefinisikan operasi-operasi FUSE yang akan digunakan.
Fungsi main() merupakan entry point dari program. Pada bagian awal, dilakukan unduhan dan ekstraksi file "rahasia.zip". Selanjutnya, dilakukan pemeriksaan argumen command line untuk melakukan registrasi pengguna atau login pengguna. Jika login berhasil, maka akan dilakukan pemanggilan fungsi countFilesByExtension() untuk menghitung jumlah file dengan ekstensi tertentu dalam folder rahasia. Terakhir, fungsi fuse_main() digunakan untuk memulai proses mounting sistem file FUSE.
Secara umum, kode tersebut bertujuan untuk membuat sebuah sistem file virtual yang terhubung ke direktori "rahasia" dengan fitur-fitur seperti autentikasi pengguna, menghitung jumlah file dengan ekstensi tertentu, dan operasi-operasi dasar pada file dan direktori.

#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <fuse_opt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <openssl/opensslv.h>
#include <openssl/evp.h>
#include <openssl/md5.h>

static const char *hidden_folder_path = "/home/testing/shift4/rahasia";
static const char *user_file_path = "/home/testing/shift4/users.txt";
static const char *result_file_path = "/home/testing/shift4/result.txt";
static const char *extension_file_path = "/home/testing/shift4/extension.txt";
static const char *extensions[] = {".gif", ".mp3", ".pdf", ".png", ".jpg", ".txt"};

static const int num_extensions = sizeof(extensions) / sizeof(extensions[0]);
//static int num_renamed_folders = 0;
static int is_authenticated = 0;  // Flag untuk menandakan apakah pengguna sudah login atau belum

static int is_user_file_exist() {
    if (access(user_file_path, F_OK) != -1) {
        // code when file exists
        return 1;
    }
    else {
        // code when file doesn't exist
        return 0;
    }
}


char* md5Make(const char* str) {
    unsigned char digest[MD5_DIGEST_LENGTH];
    EVP_MD_CTX* mdctx;
    const EVP_MD* md = EVP_md5();
    mdctx = EVP_MD_CTX_create();  // changed from EVP_MD_CTX_new
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, str, strlen(str));
    EVP_DigestFinal_ex(mdctx, digest, NULL);
    EVP_MD_CTX_destroy(mdctx);  // changed from EVP_MD_CTX_free

    char* hashedStr = (char*)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
        sprintf(&hashedStr[i * 2], "%02x", (unsigned int)digest[i]);

    return hashedStr;
}
int isFileDownloaded(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file) {
        fclose(file);
        return 1;  // File exists
    }
    return 0;  // File doesn't exist
}

char downloadAndUnzip() {
    if (isFileDownloaded("rahasia.zip")) {
        printf("rahasia.zip already exists.\n");
    } else {
        char url[] = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes";
        char cmd[200] = "wget -O rahasia.zip '";
        strcat(cmd, url);
        strcat(cmd, "'");
        system(cmd);  // Download the zip file

        system("rm -rf rahasia");  // Remove the existing 'rahasia' directory
        system("unzip rahasia.zip");  // Extract the zip file
    }
    return 0;
}


// Fungsi untuk melakukan registrasi pengguna baru
static int register_user() {
    char username[100];
    char password[100];

    printf("Username: ");
    scanf("%s", username);
    printf("Password: ");
    scanf("%s", password);

    // Hash password menggunakan fungsi md5Make
    char* hashedPassword = md5Make(password);

    // Menulis username dan hashed password ke dalam file users.txt
    FILE *user_file = fopen(user_file_path, "a");
    if (user_file == NULL) {
        printf("Error: Cannot open user file.\n");
        return -1;
    }
    fprintf(user_file, "%s:%s\n", username, hashedPassword);
    fclose(user_file);

    printf("Registration successful.\n");
    return 0;
}


// Fungsi untuk melakukan login
static int login() {
    char username[100];
    char password[100];

    printf("Username: ");
    scanf("%s", username);
    printf("Password: ");
    scanf("%s", password);

    // Membaca username dan hashed password dari file users.txt
    FILE *user_file = fopen(user_file_path, "r");
    if (user_file == NULL) {
        printf("Error: Cannot open user file.\n");
        return -1;
    }

    char line[256];
    is_authenticated = 0; // Mengubah nilai variabel is_authenticated yang ada di luar fungsi
    while (fgets(line, sizeof(line), user_file)) {
        char stored_username[100];
        char stored_password[100];
        sscanf(line, "%[^:]:%s", stored_username, stored_password);

        // Memeriksa kecocokan username
        if (strcmp(username, stored_username) == 0) {
            is_authenticated = 1;
            break;
        }
    }

    fclose(user_file);

    if (is_authenticated) {
        printf("Login successful.\n");
        return 0;
    } else {
        printf("Invalid username or password.\n");
        return -1;
    }
}


// Fungsi rekursif untuk menghitung jumlah file berdasarkan ekstensi
static void count_files_recursive(const char *path, int *count) {
    DIR *dp = opendir(path);
    if (dp == NULL) {
        printf("Error: Cannot open folder %s.\n", path);
        return;
    }

    struct dirent *de;
    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        char file_path[256];
        sprintf(file_path, "%s/%s", path, de->d_name);

        if (de->d_type == DT_DIR) { // Memeriksa apakah entri adalah direktori
            count_files_recursive(file_path, count);
        } else if (de->d_type == DT_REG) { // Memeriksa apakah entri adalah file
            char *filename = de->d_name;
            char *extension = strrchr(filename, '.'); // Mengambil ekstensi file dengan mencari titik terakhir

            if (extension != NULL) {
                for (int i = 0; i < num_extensions; i++) {
                    if (strcmp(extension, extensions[i]) == 0) {
                        count[i]++;
                        break;
                    }
                }
            }
        }
    }

    closedir(dp);
}

// Fungsi untuk menghitung jumlah file berdasarkan ekstensi
static void count_files_by_extension() {
    int count[num_extensions];
    memset(count, 0, sizeof(count)); // Inisialisasi semua elemen count menjadi 0

    count_files_recursive(hidden_folder_path, count);

    // Menyimpan hasil perhitungan ke dalam file extension.txt
    FILE *extension_file = fopen(extension_file_path, "w");
    if (extension_file == NULL) {
        printf("Error: Cannot create extension file.\n");
        return;
    }

    for (int i = 0; i < num_extensions; i++) {
        fprintf(extension_file, "%s: %d\n", extensions[i], count[i]);
    }

    fclose(extension_file);

    printf("File count by extension saved to extension.txt.\n");
}

static int custom_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char full_path[256];
    snprintf(full_path, sizeof(full_path), "%s%s", hidden_folder_path, path);

    res = lstat(full_path, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static int custom_rename(const char *from, const char *to)
{
    if (!is_authenticated) {
        printf("Please log in first.\n");
        return -1;
    }

    // Extract the folder or file name from the 'from' path
    char name[256];
    sscanf(from, "/%[^/]", name);

    // Extract the group code from the 'to' path
    char group_code[256];
    sscanf(to, "/%[^_]", group_code);

    // Construct the new folder or file name with the desired format
    char new_name[256];
    sprintf(new_name, "/%s_%s", group_code, name);

    // Construct the old and new paths
    char old_path[256];
    sprintf(old_path, "%s%s", hidden_folder_path, from);

    char new_path[256];
    sprintf(new_path, "%s%s", hidden_folder_path, to); // Use 'to' directly

    struct stat st;
    stat(old_path, &st);
    int is_directory = S_ISDIR(st.st_mode);

    int res;
    if (is_directory) {
        res = rename(old_path, new_path);
        if (res == -1)
            return -errno;

        // Rename folder recursively
        DIR *dp = opendir(new_path); // Use 'new_path'
        if (dp == NULL)
            return -errno;

        struct dirent *de;
        while ((de = readdir(dp)) != NULL) {
            if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
                continue;

            char old_sub_path[256];
            sprintf(old_sub_path, "%s/%s", old_path, de->d_name);

            char new_sub_path[256];
            sprintf(new_sub_path, "%s/%s", new_path, de->d_name);

            res = custom_rename(old_sub_path, new_sub_path);
            if (res == -1)
                return -errno;
        }

        closedir(dp);
    } else {
        // Create the parent directory of the new path if it doesn't exist
        char parent_dir[256];
        strncpy(parent_dir, new_path, sizeof(parent_dir));
        parent_dir[sizeof(parent_dir) - 1] = '\0';
        char *parent = dirname(parent_dir);
        mkdir(parent, 0755);

        res = rename(old_path, new_path);
        if (res == -1)
            return -errno;
    }

    // Collect the paths of renamed files/folders
    FILE *result_file = fopen(result_file_path, "a");
    if (result_file == NULL) {
        printf("Error: Cannot open the result file.\n");
        return -1;
    }
    fprintf(result_file, "%s -> %s\n", old_path, new_path);
    fclose(result_file);

    return 0;
}


static int custom_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char full_path[256];
    snprintf(full_path, sizeof(full_path), "%s%s", hidden_folder_path, path);

    DIR *dp = opendir(full_path);
    if (dp == NULL)
        return -errno;

    struct dirent *de;
    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);
    return 0;
}

static int custom_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    int fd = open(hidden_folder_path, O_RDONLY);
    if (fd == -1)
        return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static struct fuse_operations custom_oper = {
    .getattr = custom_getattr,
    .readdir = custom_readdir,
    .read = custom_read,
    .rename = custom_rename,
};

int main(int argc, char *argv[])
{
    downloadAndUnzip();
    umask(0);

    // Memeriksa apakah file users.txt sudah ada
    int user_file_exist = is_user_file_exist();
    if (!user_file_exist) {
        printf("User file not found. Creating new user file.\n");
        FILE *user_file = fopen(user_file_path, "w");
        if (user_file == NULL) {
            printf("Error: Cannot create user file.\n");
            return -1;
        }
        fclose(user_file);
    }

    // Memeriksa argumen command line
// Memeriksa argumen command line
if (argc < 3) {
    printf("Usage: %s <mount-point> <-r|-l>\n", argv[0]);
    return -1;
}

if (strcmp(argv[2], "-r") == 0) {
    printf("Registering new user...\n");
    register_user();
    return 0;
} else if (strcmp(argv[2], "-l") == 0) {
    printf("Logging in...\n");
    if (login() == 0) {
        printf("Login successful. Mounting...\n");

        // Construct the fuse_args structure and pass it to fuse_main
        struct fuse_args args = FUSE_ARGS_INIT(0, NULL);
        fuse_opt_add_arg(&args, argv[0]);
        fuse_opt_add_arg(&args, argv[1]);
        fuse_opt_add_arg(&args, "-o");
        fuse_opt_add_arg(&args, "nonempty");

        // Hitung jumlah file berdasarkan ekstensi dan simpan ke extension.txt
        count_files_by_extension();

        // Mount file system menggunakan custom_oper
        return fuse_main(args.argc, args.argv, &custom_oper, NULL);
    } else {
        printf("Login failed. Exiting...\n");
        return -1;
    }
} else {
    printf("Invalid argument. Usage: %s <mount-point> <-r|-l>\n", argv[0]);
    return -1;

}


    return 0;
}

